"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2020-11-13 19:01:40
Language: MicroPython 1.13
Project: Example of Dobot
"""

from multiaxis import Axis, MultiAxis
from dobot import Dobot

base = Axis(dir_pin=2, step_pin=4)
rear_arm = Axis(dir_pin=16, step_pin=17)
forearm = Axis(dir_pin=5, step_pin=18)
arm = MultiAxis(base, rear_arm, forearm)

my_dobot = Dobot(arm)

# here your code using my_dobot method's
