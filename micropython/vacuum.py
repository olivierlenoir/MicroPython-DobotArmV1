"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2020-12-06 11:01:34
Language: MicroPython 1.13
Project: Vacuum pump with suction cup and release valve drive by L9110S
Description:
"""

from machine import Pin


class Vacuum(object):

    def __init__(self, ia_pump, ib_valve):
        self.pump = Pin(ia_pump, Pin.OUT)
        self.valve = Pin(ib_valve, Pin.OUT)
        self.off()

    def catch(self):
        self.pump(1)
        self.valve(0)

    def release(self):
        self.pump(0)
        self.valve(1)

    def off(self):
        self.pump(0)
        self.valve(0)
