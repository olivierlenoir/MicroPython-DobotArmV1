"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2020-11-13 13:57:19
Language: MicroPython 1.13
Project: Dobot Arm V1.0
Description: This version use MultiAxis with DRV8825
"""

# from multiaxis import Axes, MultiAxis
from cmath import polar, rect
from math import degrees, radians, pi, acos

tau = 2 * pi


def al_kashi(a, b, c):
    """In an a, b, c segment triangle, return C angle in radian"""
    return acos((a ** 2 + b ** 2 - c ** 2) / (2 * a * b))


def sub(a, b):
    return a - b


class Dobot(object):
    FULL_STEP = const(1)
    HALF_STEP = const(2)
    QUARTER_STEP = const(4)
    EIGHTH_STEP = const(8)
    SIXTEENTH_STEP = const(16)

    def __init__(self, multiaxis):
        self.dobot = multiaxis
        # base, arm and tool are [r, phi]
        self.base = [0, 0]
        self.arm = [[135, pi / 2], [160, 0]]
        self.tool = [50.9, 0]
        # stepper motor reductor
        self.stepper_steps = 200
        self.stepper_reductor = 5 + 2 / 11
        # micro stepping
        self.microstep = self.HALF_STEP
        # init stepper position
        self.dobot.axes[0].pos = self._rad2step(self.base[1])
        self.dobot.axes[1].pos = self._rad2step(self.arm[0][1])
        self.dobot.axes[2].pos = self._rad2step(self.arm[1][1])

    def _rad2step(self, rad):
        """Convert radians into machine steps"""
        return int(round(rad * self.stepper_steps * self.stepper_reductor * self.microstep / tau))

    def _deg2step(self, deg):
        """Convert degrees into machine steps"""
        return int(round(deg * self.stepper_steps * self.stepper_reductor * self.microstep / 360))

    def _step2rad(self, step):
        """Convert steps into radians"""
        return step * tau / (self.stepper_steps * self.stepper_reductor * self.microstep)

    def _update_rad_pos(self):
        """Update base and arm radian position using MutliAxis.cur_pos() as step position"""
        self.base[1], self.arm[0][1], self.arm[1][1] = map(self._step2rad, self.dobot.cur_pos())

    def initialize(self):
        """Initialization sequence"""
        pass

    def cur_rad(self):
        """Current angles base, arm in radian"""
        return tuple([self.base[1]] + [jnt[1] for jnt in self.arm] + [self.tool[1]])

    def cur_deg(self):
        """Current angles base, arm in degree"""
        return tuple(map(degrees, self.cur_rad()))

    def cur_mm(self):
        """Current position in mm"""
        arm = sum(rect(*ar) for ar in self.arm) + rect(*self.tool)
        base = rect(arm.real, self.base[1])
        return (base.real, base.imag, arm.imag)

    def move_to_rad(self, *rad):
        """Absolute angular movement in radian, expect as many values as axis"""
        self.dobot.g01(*(map(self._rad2step, rad)))
        self._update_rad_pos()

    def move_rad(self, *rad):
        """Relative angular movement in radian, expect as many values as axis"""
        self.move_to_rad(*(map(sum, zip(self.cur_rad(), rad))))

    def move_to_deg(self, *deg):
        """Absolute angular movement in degree, expect as many values as axis"""
        self.dobot.g01(*(map(self._deg2step, deg)))
        self._update_rad_pos()

    def move_deg(self, *deg):
        """Relative angular movement in degree, expect as many values as axis"""
        self.move_to_deg(*(map(sum, zip(self.cur_deg(), deg))))

    def move_to_mm(self, x, y, z):
        """Absolute movement in mm, expect three-dimensional values"""
        r_xy, phi_0 = polar(complex(x, y))
        r_arm, phi_arm = polar(complex(r_xy, z) - rect(*self.tool))
        phi_1 = al_kashi(self.arm[0][0], r_arm, self.arm[1][0]) + phi_arm
        phi_2 = phi_1 - pi + al_kashi(self.arm[0][0], self.arm[1][0], r_arm)
        self.move_to_rad(phi_0, phi_1, phi_2)

    def move_mm(self, x, y, z):
        """Relative movement in mm, expect three-dimensional values"""
        self.move_to_mm(*(map(sum, zip(self.cur_mm(), (x, y, z)))))

    def g01(self, x, y, z, segment=1):
        """G-code G01 - Linear interpolation in mm made of segment in mm"""
        dx, dy, dz = map(sub, (x, y, z), self.cur_mm())
        distance = (dx ** 2 + dy ** 2 + dz ** 2) ** 0.5
        segments = distance // segment
        seg = range(1, segments + 1)
        for s in seg:
            ratio = s / segments - 1
            self.move_to_mm(x + dx * ratio, y + dy * ratio, z + dz * ratio)
